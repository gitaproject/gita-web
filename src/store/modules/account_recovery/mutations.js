export default {
  setUsername (state, username) { state.username = username },
  setisValid (state, isValid) { state.isValid = isValid },
  setuuid (state, uuid) { state.uuid = uuid },
  settoken (state, token) { state.token = token },
  setpassword (state, password) { state.password = password },
  setpasswordRepeat (state, passwordRepeat) { state.passwordRepeat = passwordRepeat }
}
