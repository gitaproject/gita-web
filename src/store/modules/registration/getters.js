export default {
  registerInfo: state => state.registerInfo,
  resendTokenInfo: state => state.resendTokenInfo,
  isValid: state => state.isValid,
  username: state => state.username,
  phone: state => state.phone,
  firstName: state => state.firstName,
  lastName: state => state.lastName,
  password: state => state.password,
  birthday: state => state.birthday,
  token: state => state.token
}
