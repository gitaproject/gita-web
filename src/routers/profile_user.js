export default [
  { path: '', name: 'profileUser', redirect: 'wall' },
  {
    path: 'wall',
    name: 'profileUserWall',
    component: () => import(/* webpackChunkName: "profileUser" */ '@/components/profile_user/Wall.vue')
  },
  {
    path: 'about',
    name: 'profileUserAbout',
    component: () => import(/* webpackChunkName: "profileUser" */ '@/components/profile_user/About.vue')
  },
  {
    path: 'friends',
    name: 'profileUserFriends',
    component: () => import(/* webpackChunkName: "profileUser" */ '@/components/profile_user/Friends.vue')
  }
]
