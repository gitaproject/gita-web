export default {
  username: state => state.username,
  isValid: state => state.isValid,
  uuid: state => state.uuid,
  token: state => state.token,
  password: state => state.password,
  passwordRepeat: state => state.passwordRepeat
}
