import axios from 'axios'
import { requestHandler } from '@/utils/apiHandler'
// import { responseHandler, errorResponseHandler } from '@/utils/apiHandler'

export default () => {
  let HTTP = axios.create({
    baseURL: process.env.VUE_APP_BASE_API_URL,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  })
  HTTP.interceptors.request.use(requestHandler)
  // HTTP.interceptors.response.use(null, errorResponseHandler)
  return HTTP
}
