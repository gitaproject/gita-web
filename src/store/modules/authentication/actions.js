import router from '@/routers'
import HTTP from '@/utils/apiBase'

export default {
  async doLogin ({ commit, state }) {
    let data = {
      'username': state.username,
      'password': state.password
    }
    commit('Application/startLoading', {}, { root: true })
    await HTTP().post(`/auth/login/`, data).then(
      response => {
        commit('setLogged', response.data)
        state.user = response.data.user
        router.push({ name: 'feedActivities' })
      }
    ).catch(
      e => {
        if (e.response.data.error) commit('setLoginInfo', e.response.data.error[0])
        router.push({ name: 'login' })
      }
    ).finally(() => {
      commit('Application/endLoading', {}, { root: true })
    })
  },
  async doLogout ({ commit }) {
    commit('Application/startLoading', {}, { root: true })
    await HTTP().post(`/auth/logout/`).then(
      response => {
        commit('setLogout')
        router.push({ name: 'home' })
      }
    ).catch(
      e => {
        commit('setLogout')
        router.push({ name: 'home' })
      }
    ).finally(() => {
      commit('Application/endLoading', {}, { root: true })
    })
  },
  async checkIsLogged ({ commit, state }) {
    commit('checkLogged')
    if (state.isLogged) {
      commit('Application/startLoading', {}, { root: true })
      await HTTP().post(`/auth/verify/`).then(
        response => {
          commit('setLogged', response.data)
          router.push({ name: 'home' })
        }
      ).catch(
        e => {
          commit('setLogout')
          router.push({ name: 'home' })
        }
      ).finally(() => {
        commit('Application/endLoading', {}, { root: true })
      })
    }
  }
}
