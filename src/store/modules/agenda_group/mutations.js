export default {
  setActivity (state, activity) {
    state.activityId = activity['activity']['id']
    state.userActivityIsActivityValid = activity['is_activity_valid']
    state.activityDuration = activity['activity']['duration']
    state.activityTimeStart = activity['activity']['time_start']
    state.activityActivityName = activity['activity']['activity_name']
    state.activityDescription = activity['activity']['description']
    state.activityGroup = activity['activity']['group']
    state.activityLocation = activity['activity']['location']
    state.activityAgenda = activity['activity']['agenda']
    state.activityCreatedAt = activity['activity']['created_at']
    state.activityUpdatedAt = activity['activity']['updated_at']
  },
  setSampleState (state, sampleState) { state.sampleState = sampleState }
}
