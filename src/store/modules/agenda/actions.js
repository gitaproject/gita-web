import HTTP from '@/utils/apiBase'
import router from '@/routers'

export default {
  async generatePersonalAgenda ({ commit, state, dispatch }) {
    await HTTP().get(`/agenda/generate/${state.agendaId}/`).then(
      response => {
        commit('resetPersonalAgendas')
        dispatch('readPersonalAgendas')
      }
    ).catch(
      e => {
        if (!state.personalAgendasNoMore) commit('Application/setSnackbarText', 'Fail to generate agenda.', { root: true })
      }
    )
  },
  async readPersonalAgendas ({ commit, state }) {
    if (state.personalAgendasIsLoading || state.personalAgendasNoMore) return
    commit('Application/startLoading', {}, { root: true })
    commit('setPersonalAgendasLoading', true)
    await HTTP().get(`/agenda/?page=${state.personalAgendasPage + 1}`).then(
      response => {
        commit('addPersonalAgendas', response.data)
        state.personalAgendasNoMore = !response.data['next']
      }
    ).catch(
      e => {
        commit('setPersonalAgendasLoading', false)
        state.personalAgendasNoMore = e.response.status === 404
        if (!state.personalAgendasNoMore) commit('Application/setSnackbarText', 'Fail to retrieve agendas.', { root: true })
      }
    ).finally(() => {
      commit('Application/endLoading', {}, { root: true })
    })
  },
  async createPersonalAgenda ({ commit, state, dispatch }) {
    commit('Application/startLoading', {}, { root: true })
    let data = {
      agenda_name: state.agendaName,
      description: state.description,
      loop: state.loop,
      range_time_start: state.rangeTimeStart,
      range_time_until: state.rangeTimeUntil
    }
    if (state.loop) data.loop_interlude = state.loopInterlude
    if (state.locations) data.locations = state.locations.map(location => ({ location: location }))
    let activities = []
    for (let i = 0; i < state.activities.length; i++) {
      activities[i] = {
        activity_name: state.activities[i].activity_name,
        description: state.activities[i].description,
        duration: state.activities[i].duration,
        hosts: state.activities[i].hosts.map(host => ({ host: host.from_user.id })),
        members: state.activities[i].members.map(member => ({ member: member.from_user.id }))
      }
      if (state.activities[i].location) activities[i].location = state.activities[i].location.id
    }
    data.activities = activities
    await HTTP().post(`/agenda/`, data).then(
      response => {
        commit('resetAgendaForm')
        router.push({ name: 'feedAgendas' })
      }
    ).catch(
      e => {
        commit('Application/setSnackbarText', 'Fail to create new agenda, please check your data.', { root: true })
      }
    ).finally(() => {
      commit('Application/endLoading', {}, { root: true })
    })
  },
  async updatePersonalAgenda ({ commit, state, dispatch }) {
    let data = {
      'agenda_name': state.agendaName,
      'description': state.description,
      'loop': state.loop,
      'range_time_start': state.rangeTimeStart,
      'range_time_until': state.rangeTimeUntil,
      'activities': state.activities
    }
    if (state.loop) data['loop_interlude'] = state.loopInterlude
    if (state.locations) data['locations'] = state.locations.map(location => ({ 'location': location }))

    await HTTP().patch(`/agenda/detail/${state.agendaId}/`, data).then(
      response => {
        commit('resetPersonalAgendas')
        dispatch('readPersonalAgendas')
      }
    ).catch(
      e => {
        console.log(e.response.data)
      }
    )
  },
  async deletePersonalAgenda ({ commit, state, dispatch }) {
    await HTTP().delete(`/agenda/detail/${state.agendaId}/`).then(
      response => {
        commit('resetPersonalAgendas')
        dispatch('readPersonalAgendas')
      }
    ).catch(
      e => {
        console.log(e.response.data)
      }
    )
  }
}
