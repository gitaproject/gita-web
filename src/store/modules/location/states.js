export default {
  locationsNoMore: false,
  locations: [],
  locationsIsLoading: false,
  locationsPage: 0,
  locationsSearch: null
}
