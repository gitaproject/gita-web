export default {
  setPersonalAgendasLoading (state, value) {
    state.personalAgendasIsLoading = value
  },
  addPersonalAgendas (state, personalAgendas) {
    state.personalAgendas = state.personalAgendas.concat(personalAgendas.results)
    state.personalAgendasPage += 1
    state.personalAgendasIsLoading = false
  },
  resetPersonalAgendas (state) {
    state.personalAgendas = []
    state.personalAgendasPage = 0
    state.personalAgendasIsLoading = false
    state.personalAgendasNoMore = false
  },

  addActivity (state) {
    state.activities.push({
      activity_name: '',
      description: '',
      duration: 1,
      location: null,
      hosts: [],
      members: []
    })
  },
  removeActivity (state, index) {
    state.activities.splice(index, 1)
  },
  setActivityName (state, payload) { state.activities[payload.index].activity_name = payload.value },
  setActivityDescription (state, payload) { state.activities[payload.index].description = payload.value },
  setActivityDuration (state, payload) { state.activities[payload.index].duration = payload.value },
  setActivityLocation (state, payload) { state.activities[payload.index].location = payload.value },
  setActivityHosts (state, payload) { state.activities[payload.index].hosts = payload.value },
  setActivityMembers (state, payload) { state.activities[payload.index].members = payload.value },

  setAgendaId (state, agendaId) { state.agendaId = agendaId },
  setAgendaName (state, agendaName) { state.agendaName = agendaName },
  setDescription (state, description) { state.description = description },
  setLoop (state, loop) { state.loop = loop },
  setRangeTimeStart (state, rangeTimeStart) { state.rangeTimeStart = rangeTimeStart },
  setRangeTimeUntil (state, rangeTimeUntil) { state.rangeTimeUntil = rangeTimeUntil },
  setActivities (state, activities) { state.activities = activities },
  setLoopInterlude (state, loopInterlude) { state.loopInterlude = loopInterlude },
  setLocations (state, locations) { state.locations = locations },

  resetAgendaForm (state) {
    state.agendaId = 0
    state.agendaName = ''
    state.description = ''
    state.loop = 0
    state.rangeTimeStart = new Date().toISOString()
    state.rangeTimeUntil = new Date().toISOString()
    state.activities = [
      {
        activity_name: '',
        description: '',
        duration: 1,
        location: null,
        hosts: [],
        members: []
      }
    ]
    state.loopInterlude = 0
    state.locations = []
  }
}
