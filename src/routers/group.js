export default [
  {
    path: 'discover',
    name: 'groupDiscover',
    component: () => import(/* webpackChunkName: "groups" */ '@/components/group/GroupDiscover.vue')
  },
  {
    path: 'create',
    name: 'groupCreate',
    component: () => import(/* webpackChunkName: "groups" */ '@/components/group/GroupCreate.vue')
  },
  {
    path: 'list',
    name: 'groupList',
    component: () => import(/* webpackChunkName: "groups" */ '@/components/group/GroupList.vue')
  }
]
