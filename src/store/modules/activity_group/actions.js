import HTTP from '@/utils/apiBase'

export default {
  async readGroupActivities ({ commit, state }) {
    if (state.groupActivitiesIsLoading) return
    commit('setGroupActivitiesLoading', true)
    await HTTP().get(`/agenda/activity/?page=${state.groupActivitiesPage + 1}`).then(
      response => {
        commit('addGroupActivities', response.data)
      }
    ).catch(
      e => {
        commit('setGroupActivitiesLoading', false)
        console.log(e.response.data)
      }
    )
  },
  async retrieveGroupActivity ({ commit, state, dispatch }) {
    await HTTP().get(`/agenda/activity/group/detail/${state.groupActivityId}/`).then(
      response => {
        commit('setGroupActivity', response.data)
      }
    ).catch(
      e => {
        console.log(e.response.data)
      }
    )
  },
  async updateGroupActivity ({ commit, state, dispatch }) {
    let data = {
      'activity': {
        'time_start': state.timeStart,
        'duration': state.duration,
        'activity_name': state.activityName,
        'description': state.description
      }
    }
    await HTTP().patch(`/agenda/activity/${state.activityId}/`, data).then(
      response => {
        commit('resetGroupActivities')
        dispatch('readGroupActivities')
      }
    ).catch(
      e => {
        commit('setGroupActivitiesLoading', false)
        console.log(e.response.data)
      }
    )
  },
  async deleteGroupActivity ({ commit, state, dispatch }) {
    await HTTP().delete(`/agenda/activity/${state.activityId}/`).then(
      response => {
        commit('resetGroupActivities')
        dispatch('readGroupActivities')
      }
    ).catch(
      e => {
        commit('setGroupActivitiesLoading', false)
        console.log(e.response.data)
      }
    )
  }
}
