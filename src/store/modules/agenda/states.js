export default {
  personalAgendasNoMore: false,
  personalAgendas: [],
  personalAgendasIsLoading: false,
  personalAgendasPage: 0,

  agendaId: 0,
  agendaName: '',
  description: '',
  loop: 0,
  rangeTimeStart: new Date().toISOString(),
  rangeTimeUntil: new Date().toISOString(),
  activities: [
    {
      activity_name: '',
      description: '',
      duration: 1,
      location: null,
      hosts: [],
      members: []
    }
  ],
  loopInterlude: 0,
  locations: []
}
