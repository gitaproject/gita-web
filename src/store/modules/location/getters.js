export default {
  locationsNoMore: state => state.locationsNoMore,
  locations: state => state.locations,
  locationsIsLoading: state => state.locationsIsLoading,
  locationsPage: state => state.locationsPage,
  locationsSearch: state => state.locationsSearch
}
