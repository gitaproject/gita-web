export default {
  startLoading (state) {
    state.isLoading++
  },
  endLoading (state) {
    state.isLoading--
  },
  setTitle (state, title) {
    state.title = title
  },
  setDrawer (state, drawer) {
    state.drawer = drawer
  },
  revertDrawer (state) {
    state.drawer = !this.drawer
  },
  setSnackbarText (state, snackbarText) {
    state.snackbarText = snackbarText
    state.snackbar = true
  },
  setSnackbar (state, snackbar) {
    state.snackbar = snackbar
  }
}
