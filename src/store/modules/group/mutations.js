export default {
  setUsername (state, username) {
    state.username = username
  },
  setPassword (state, password) {
    state.password = password
  },
  setLoginMessage (state, loginMessage) {
    state.loginMessage = loginMessage
  },
  setLayout (state, layout) {
    state.layout = layout
  },
  setLogged (state) {
    state.isLogged = true
    state.password = ''
    state.username = ''
  },
  setLogout (state) {
    state.isLogged = false
  }
}
