export default [
  {
    path: '',
    name: 'settingMenu',
    component: () => import(/* webpackChunkName: "settings" */ '@/components/settings/Menu.vue')
  },
  {
    path: 'account',
    name: 'settingAccount',
    component: () => import(/* webpackChunkName: "settings" */ '@/components/settings/Account.vue')
  }
]
