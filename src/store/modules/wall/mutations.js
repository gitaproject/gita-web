export default {
  setUserID (state, userID) { state.userID = userID },
  setBody (state, body) { state.body = body },
  setWalls (state, walls) { state.walls = walls }
}
