import HTTP from '@/utils/apiBase'
import router from '@/routers'

export default {
  async checkUsername ({ commit, state }) {
    let data = {
      'username': state.username
    }
    await HTTP().post(`/register/check-username/`, data).then(
      response => {
        commit('setIsValid', response.data['is_valid'])
      }
    ).catch(
      e => {
        console.log(e.response.data)
      }
    )
  },
  async doRegister ({ commit, state }) {
    let data = {
      'phone': state.phone,
      'first_name': state.firstName,
      'last_name': state.lastName,
      'username': state.username,
      'password': state.password,
      'birthday': state.birthday
    }
    await HTTP().post(`/register/register/`, data).then(
      () => {
        state.resendTokenInfo = `Verification token sent to +${state.phone}`
        router.push({ name: 'verifyPhoneToken' })
      }
    ).catch(
      e => {
        state.registerInfo = e.response.data
        router.push({ name: 'registerUser' })
      }
    )
  },
  async doResendToken ({ commit, state }) {
    let data = {
      'phone': state.phone
    }
    await HTTP().post(`/register/resend-token/`, data).then(
      response => {
        console.log(response.data)
        if (response.data.phone) {
          state.resendTokenInfo = `New token sent to ${response.data.phone}`
        } else {
          state.resendTokenInfo = response.data
        }
      }
    ).catch(
      e => {
        console.log(e.response)
        state.resendTokenInfo = e.response.data[0]
      }
    )
  },
  async doVerifyPhone ({ commit, state, dispatch }) {
    let data = {
      'phone': state.phone,
      'phone_token': state.token
    }
    await HTTP().post(`/register/verify-phone/`, data).then(
      response => {
        let payload = {
          'username': state.username,
          'password': state.password
        }
        commit('Authentication/setUsernameAndPassword', payload, { root: true })
        dispatch('Authentication/doLogin', null, { root: true })
      }
    ).catch(
      e => {
        state.resendTokenInfo = e.response.data.phone_token[0]
      }
    )
  }
}
