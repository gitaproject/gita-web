import HTTP from '@/utils/apiBase'
import router from '@/routers'

export default {
  async getExplore ({ state, rootState, commit }) {
    commit('Application/startLoading', {}, { root: true })
    await HTTP().get(`/social/explore/?search=${state.searchKey}`).then(
      response => {
        commit('setExplore', response.data.results)
      }
    ).catch(
      e => {
        router.push({ name: 'feed' })
      }
    ).finally(() => {
      commit('Application/endLoading', {}, { root: true })
    })
  }
}
