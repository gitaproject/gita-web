import HTTP from '@/utils/apiBase'
import router from '@/routers'

export default {
  async getProfileByUsername ({ state, commit }) {
    commit('Application/startLoading', {}, { root: true })
    await HTTP().get(`/social/profile/${state.username}/`).then(
      response => {
        state.otherUser = response.data
      }
    ).catch(
      e => {
        router.push({ name: 'feed' })
      }
    ).finally(() => {
      commit('Application/endLoading', {}, { root: true })
    })
  }
}
