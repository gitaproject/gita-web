import HTTP from '@/utils/apiBase'
import router from '@/routers'

export default {
  async postWall ({ state, commit }) {
    commit('Application/startLoading', {}, { root: true })
    let data = {
      body: state.body
    }
    await HTTP().post(`/social/wall/${state.userID}/`, data).then(
      response => {
        state.body = ''
        router.push({ name: 'profilePersonalWall' })
      }
    ).catch(
      e => {
        commit('Application/setSnackbarText', e.response.data.error, { root: true })
      }
    ).finally(() => {
      commit('Application/endLoading', {}, { root: true })
    })
  },
  async getUserWall ({ state, commit }) {
    commit('Application/startLoading', {}, { root: true })
    await HTTP().get(`/social/wall/${state.userID}/`).then(
      response => {
        state.walls = response.data.results
      }
    ).finally(() => {
      commit('Application/endLoading', {}, { root: true })
    })
  },
  async doSomething ({ state, rootState, commit, dispatch, getters, rootGetters }) {
    let data = {
      'key': 'value'
    }
    await HTTP().post(`/api/url/`, data).then(
      response => {
        // get state
        let samplePayload = state.sampleState
        let sampleOtherPayload = rootState['ModuleNamespace/somethingElse']
        // retrieve state using getters
        samplePayload = getters.sampleState
        sampleOtherPayload = rootGetters['ModuleNamespace/somethingElse']
        // call mutation
        commit('ModuleNamespace/setSomethingElse', samplePayload, { root: true })
        // call action
        dispatch('ModuleNamespace/doSomethingElse', sampleOtherPayload, { root: true })
      }
    ).catch(
      e => {
        router.push({ name: 'feed' })
      }
    )
  }
}
