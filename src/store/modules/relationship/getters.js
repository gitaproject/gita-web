export default {
  friendsNoMore: state => state.friendsNoMore,
  friends: state => state.friends,
  friendsIsLoading: state => state.friendsIsLoading,
  friendsPage: state => state.friendsPage,
  friendsSearch: state => state.friendsSearch,

  friendRequests: state => state.friendRequests,
  friendUsername: state => state.friendUsername
}
