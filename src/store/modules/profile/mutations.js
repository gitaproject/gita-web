export default {
  setUserProfile (state, userProfile) {
    state.userId = userProfile['id']
    state.profileId = userProfile['profile']['id']
    state.createdAt = userProfile['profile']['created_at']
    state.updatedAt = userProfile['profile']['updated_at']
    state.birthday = userProfile['profile']['birthday']
    state.gender = userProfile['profile']['gender']
    state.about = userProfile['profile']['about']
    state.avatar = userProfile['profile']['avatar']
    state.cover = userProfile['profile']['cover']
    state.lastLogin = userProfile['last_login']
    state.username = userProfile['username']
    state.lastName = userProfile['last_name']
    state.email = userProfile['email']
    state.isActive = userProfile['is_active']
    state.dateJoined = userProfile['date_joined']
    state.phone = userProfile['phone']
    state.firstName = userProfile['first_name']
    state.phoneVerified = userProfile['phone_verified']
    state.emailVerified = userProfile['email_verified']
  },
  setAvatar (state, avatar) { state.userId = avatar },
  setCover (state, cover) { state.userId = cover }
}
