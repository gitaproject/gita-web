export default [
  {
    path: '',
    name: 'registerUser',
    component: () => import(/* webpackChunkName: "register" */ '@/components/registration/RegisterUser.vue')
  },
  {
    path: 'verify',
    name: 'verifyPhoneToken',
    component: () => import(/* webpackChunkName: "register" */ '@/components/registration/VerifyPhone.vue')
  }
]
