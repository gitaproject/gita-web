export default {
  setOtherUser (state, otherUser) { state.otherUser = otherUser },
  setUsername (state, username) { state.username = username }
}
