export default [
  { path: '', name: 'profile', redirect: 'personal' },
  {
    path: 'personal',
    name: 'profilePersonalWall',
    component: () => import(/* webpackChunkName: "profile" */ '@/components/profile/Wall.vue')
  },
  {
    path: 'schedule',
    name: 'profilePersonalSchedule',
    component: () => import(/* webpackChunkName: "profile" */ '@/components/profile/Schedule.vue')
  },
  {
    path: 'about',
    name: 'profilePersonalAbout',
    component: () => import(/* webpackChunkName: "profile" */ '@/components/profile/About.vue')
  },
  {
    path: 'friends',
    name: 'profilePersonalFriends',
    component: () => import(/* webpackChunkName: "profile" */ '@/components/profile/Friends.vue')
  }
]
