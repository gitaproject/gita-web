export default [
  {
    path: 'discover',
    name: 'locationDiscover',
    component: () => import(/* webpackChunkName: "locations" */ '@/components/location/LocationDiscover.vue')
  },
  {
    path: 'create',
    name: 'locationCreate',
    component: () => import(/* webpackChunkName: "locations" */ '@/components/location/LocationCreate.vue')
  },
  {
    path: 'list',
    name: 'locationList',
    component: () => import(/* webpackChunkName: "locations" */ '@/components/location/LocationList.vue')
  }
]
