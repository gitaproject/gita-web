export default {
  userID: state => state.userID,
  body: state => state.body,
  walls: state => state.walls
}
