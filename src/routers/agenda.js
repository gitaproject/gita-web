export default [
  {
    path: '',
    name: 'agendaList',
    component: () => import(/* webpackChunkName: "agenda" */ '@/components/agenda/List.vue')
  }
]
