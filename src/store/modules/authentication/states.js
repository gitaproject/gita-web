export default {
  isLogged: false,
  user: {
    profile: {
      id: null,
      deleted: null,
      created_at: null,
      updated_at: null,
      birthday: null,
      gender: null,
      about: null,
      avatar: null,
      cover: null,
      user: null
    },
    id: null,
    phone: null,
    username: null,
    first_name: null,
    last_name: null,
    full_name: null,
    email: null,
    last_login: null,
    is_active: null,
    date_joined: null,
    phone_verified: null,
    email_verified: null,
    member_of_groups: null,
    administrated_locations: null
  },
  username: '',
  password: '',
  loginInfo: ''
}
