export default {
  setFriendsLoading (state, value) {
    state.friendsIsLoading = value
  },
  setFriendsSearch (state, value) {
    state.friendsSearch = value
  },
  addFriends (state, friends) {
    state.friends = state.friends.concat(friends.results)
    state.friendsPage += 1
    state.friendsIsLoading = false
  },
  resetFriends (state) {
    state.friends = []
    state.friendsPage = 0
    state.friendsIsLoading = false
    state.friendsNoMore = false
  },

  setFriendUsername (state, friendUsername) { state.friendUsername = friendUsername },
  setFriendRequests (state, friendRequests) { state.friendRequests = friendRequests },
  setFriends (state, friends) { state.friends = friends }
}
