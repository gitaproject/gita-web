import HTTP from '@/utils/apiBase'

export default {
  async getLocations ({ commit, state }) {
    commit('Application/startLoading', {}, { root: true })
    if (state.locationsIsLoading || state.locationsNoMore) return
    let params = {
      page: state.locationsPage + 1,
      search: state.locationsSearch
    }
    state.locationsIsLoading = true
    await HTTP().get(`/social/location/`, { params: params }).then(
      response => {
        commit('addLocations', response.data)
        state.locationsNoMore = !response.data.next
      }
    ).catch(
      e => {
        state.locationsNoMore = e.response.status === 404
        if (!state.locationsNoMore) commit('Application/setSnackbarText', 'Fail to retrieve locations.', { root: true })
      }
    ).finally(() => {
      state.locationsIsLoading = false
      commit('Application/endLoading', {}, { root: true })
    })
  }
}
