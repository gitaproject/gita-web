import HTTP from '@/utils/apiBase'
import router from '@/routers'

export default {
  async retrieveActivity ({ state, rootState, commit, dispatch, getters, rootGetters }) {
    await HTTP().post(`/agenda/activity/group/detail/${state.userActivityId}/`).then(
      response => {
        commit('setActivity', response.data)
      }
    ).catch(
      e => {
        console.log(e.response.data)
      }
    )
  },
  async doSomething ({ state, rootState, commit, dispatch, getters, rootGetters }) {
    let data = {
      'key': 'value'
    }
    await HTTP().post(`/api/url/`, data).then(
      response => {
        // get state
        let samplePayload = state.sampleState
        let sampleOtherPayload = rootState['ModuleNamespace/somethingElse']
        // retrieve state using getters
        samplePayload = getters.sampleState
        sampleOtherPayload = rootGetters['ModuleNamespace/somethingElse']
        // call mutation
        commit('ModuleNamespace/setSomethingElse', samplePayload, { root: true })
        // call action
        dispatch('ModuleNamespace/doSomethingElse', sampleOtherPayload, { root: true })
      }
    ).catch(
      e => {
        router.push({ name: 'feed' })
      }
    )
  }
}
