import { getToken } from '@/utils/apiAuth'

export default {
  isLogged: !!getToken(),
  username: '',
  password: '',
  loginMessage: ''
}
