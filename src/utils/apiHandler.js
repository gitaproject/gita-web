import router from '@/routers'
import store from '@/store'
// import toast from './toast'
import { getToken } from './apiAuth'

export function errorResponseHandler (error) {
  if (error.config.hasOwnProperty('errorHandle') && error.config.errorHandle === false) {
    return Promise.reject(error)
  }
  if (error.response) {
    const response = error.response

    // If type data is string
    if (typeof response.data === 'string') {
      if (response.statusText === 'Signature has expired.') {
        store.commit('setLogout')
        localStorage.clear()
        router.push({ name: 'login' })
      }
      // toast.error(response.statusText)
      return
    }

    // if type data is array
    if (Array.isArray(response.data)) {
      Array.from(response.data).forEach(item => {
        if (item === 'Signature has expired.') {
          // toast.error(item)
          store.commit('setLogout')
          localStorage.clear()
          router.push({ name: 'login' })
        }
        // toast.error(item)
      })
      return
    }

    // If type data is object
    for (var key in response.data) {
      // If type data is string
      if (typeof (response.data[key]) === 'string') {
        if (response.data[key] === 'Signature has expired.') {
          // toast.error(`Field ${key}, ${response.data[key]}`)
          store.commit('setLogout')
          localStorage.clear()
          router.push({ name: 'login' })
          return
        }
        // toast.error(`Field ${key}, ${response.data[key]}`)
        continue
      }

      if (Array.isArray(error.response.data[key])) {
        Array.from(response.data[key]).forEach(item => {
          if (item === 'Signature has expired.') {
            // toast.error(item)
            store.commit('setLogout')
            localStorage.clear()
            router.push({ name: 'login' })
          }
          // toast.error(`Field ${key}, ${item}`)
        })
      }
    }
  }
}

export function requestHandler (config) {
  // Get token from function getToken
  let token = getToken()
  // Check token not null value
  if (token) {
    config.headers.Authorization = `Token ${token}`
  }
  return config
}
