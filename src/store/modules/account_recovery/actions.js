import HTTP from '@/utils/apiBase'

export default {
  async doRecoverAccount ({ commit, state }) {
    let data = {
      'username': state.username
    }
    await HTTP().post(`/account_retrieval/recovery/`, data).then(
      response => {
        // commit('setLayout', 'auth-layout')
        // router.push({ name: 'feed' })
      }
    ).catch(
      e => {
        console.log(e.response.data)
      }
    )
  },
  async doValidateRecovery ({ commit, state }) {
    let data = {
      'uuid': state.uuid,
      'token': state.token
    }
    await HTTP().post(`/account_retrieval/validate-recovery/`, data).then(
      response => {
        state.isValid = response.data['is_valid']
      }
    ).catch(
      e => {
        state.isValid = false
      }
    )
  },
  async doResetPassword ({ commit, state, dispatch }) {
    let data = {
      'uuid': state.uuid,
      'token': state.token,
      'password': state.password
    }
    await HTTP().post(`/account_retrieval/set-password/`, data).then(
      response => {
        let payload = {
          'username': state.username,
          'password': state.password
        }
        commit('Authentication/setUsernameAndPassword', payload, { root: true })
        dispatch('Authentication/doLogin', null, { root: true })
      }
    ).catch(
      e => {
        console.log(e.response.data)
      }
    )
  }
}
