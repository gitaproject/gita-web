export default {
  user: state => state.user,
  isLogged: state => state.isLogged,
  loginInfo: state => state.loginInfo
}
