export default {
  groupActivities: [],
  groupActivitiesIsLoading: false,
  groupActivitiesPage: 0,

  activityId: null,
  timeStart: new Date().toISOString(),
  duration: 0,
  activityName: '',
  description: ''
}
