import { setToken, removeToken, getToken } from '@/utils/apiAuth'

export default {
  setUsernameAndPassword (state, { username, password }) {
    state.username = username
    state.password = password
  },
  setUsername (state, username) {
    state.username = username
  },
  setPassword (state, password) {
    state.password = password
  },
  setLoginInfo (state, loginInfo) {
    state.loginInfo = loginInfo
  },
  setLayout (state, layout) {
    state.layout = layout
  },
  setLogged (state, data) {
    setToken(data.token)
    state.user = data.user
    state.isLogged = true
    state.password = ''
    state.username = ''
  },
  setLogout (state) {
    removeToken()
    state.isLogged = false
  },
  checkLogged (state) {
    state.isLogged = !!getToken()
  }
}
