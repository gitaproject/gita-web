import Vue from 'vue'
import App from './App.vue'
import router from './routers'
import store from './store'
import Vuetify from 'vuetify'
import { Ripple } from 'vuetify/lib/directives'
import * as filters from './filters'
import './registerServiceWorker'
import 'vuetify/dist/vuetify.min.css'
import infiniteScroll from 'vue-infinite-scroll'

Vue.use(Vuetify)
Vue.use(Vuetify, {
  iconfont: 'md',
  directives: {
    Ripple
  }
})
Vue.use(infiniteScroll)
Vue.config.productionTip = false

Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
