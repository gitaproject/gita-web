export default {
  isLoading: state => state.isLoading,
  drawer: state => state.drawer,
  title: state => state.title,
  snackbar: state => state.snackbar,
  snackbarText: state => state.snackbarText
}
