export default {
  setRegisterInfo (state, registerInfo) {
    state.registerInfo = registerInfo
  },
  setIsValid (state, isValid) {
    state.isValid = isValid
  },
  setUsername (state, username) {
    state.username = username
  },
  setPhone (state, phone) {
    state.phone = phone
  },
  setFirstName (state, firstName) {
    state.firstName = firstName
  },
  setLastName (state, lastName) {
    state.lastName = lastName
  },
  setPassword (state, password) {
    state.password = password
  },
  setBirthday (state, birthday) {
    state.birthday = birthday
  },
  setToken (state, token) {
    state.token = token
  }
}
