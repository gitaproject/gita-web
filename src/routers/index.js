import Vue from 'vue'
import Router from 'vue-router'

import AgendaChildren from '@/routers/agenda'
import FeedChildren from '@/routers/feed'
import RegisterChildren from '@/routers/register'
import RecoveryChildren from '@/routers/recovery'
import RelationshipChildren from '@/routers/relationship'
import PostChildren from '@/routers/post'
import ProfileChildren from '@/routers/profile'
import ProfileUserChildren from '@/routers/profile_user'
import ProfileGroupChildren from '@/routers/profile_group'
import ProfileLocationChildren from '@/routers/profile_location'
import GroupChildren from '@/routers/group'
import LocationChildren from '@/routers/location'
import MessageChildren from '@/routers/message'
import ExploreChildren from '@/routers/explore'
import SettingsChildren from '@/routers/settings'
import { getToken } from '@/utils/apiAuth'

Vue.use(Router)

const baseRoute = [
  { path: '*', redirect: { name: 'home' } },
  {
    path: '/',
    name: 'home',
    meta: { requiresNotAuth: true },
    component: () => import(/* webpackChunkName: "home" */ '@/views/Home.vue')
  },
  {
    path: '/login',
    name: 'login',
    meta: {},
    component: () => import(/* webpackChunkName: "login" */ '@/views/Login.vue')
  },
  {
    path: '/register',
    meta: {},
    component: () => import(/* webpackChunkName: "register" */ '@/views/Register.vue'),
    children: RegisterChildren
  },
  {
    path: '/recovery',
    meta: {},
    component: () => import(/* webpackChunkName: "recovery" */ '@/views/Recovery.vue'),
    children: RecoveryChildren
  },
  {
    path: '/notification',
    name: 'notification',
    meta: { requiresAuth: true },
    component: () => import(/* webpackChunkName: "notification" */ '@/views/Notification.vue')
  },
  {
    path: '/messages',
    meta: { requiresAuth: true },
    component: () => import(/* webpackChunkName: "message" */ '@/views/Message.vue'),
    children: MessageChildren
  },
  {
    path: '/relationship',
    meta: { requiresAuth: true },
    component: () => import(/* webpackChunkName: "relationship" */ '@/views/Relationship.vue'),
    children: RelationshipChildren
  },
  {
    path: '/feed',
    meta: { requiresAuth: true },
    component: () => import(/* webpackChunkName: "feed" */ '@/views/Feed.vue'),
    children: FeedChildren
  },
  {
    path: '/profile',
    meta: { requiresAuth: true },
    component: () => import(/* webpackChunkName: "profile" */ '@/views/Profile.vue'),
    children: ProfileChildren
  },
  {
    path: '/agenda',
    meta: { requiresAuth: true },
    component: () => import(/* webpackChunkName: "agenda" */ '@/views/Agenda.vue'),
    children: AgendaChildren
  },
  {
    path: '/post',
    meta: { requiresAuth: true },
    component: () => import(/* webpackChunkName: "post" */ '@/views/Post.vue'),
    children: PostChildren
  },
  {
    path: '/u/:username',
    meta: { requiresAuth: true },
    component: () => import(/* webpackChunkName: "profileUser" */ '@/views/ProfileUser.vue'),
    children: ProfileUserChildren
  },
  {
    path: '/g/:username',
    meta: { requiresAuth: true },
    component: () => import(/* webpackChunkName: "profileGroup" */ '@/views/ProfileGroup.vue'),
    children: ProfileGroupChildren
  },
  {
    path: '/l/:username',
    meta: { requiresAuth: true },
    component: () => import(/* webpackChunkName: "profileLocation" */ '@/views/ProfileLocation.vue'),
    children: ProfileLocationChildren
  },
  {
    path: '/groups',
    name: 'groups',
    meta: { requiresAuth: true },
    component: () => import(/* webpackChunkName: "groups" */ '@/views/Group.vue'),
    children: GroupChildren
  },
  {
    path: '/locations',
    name: 'locations',
    meta: { requiresAuth: true },
    component: () => import(/* webpackChunkName: "locations" */ '@/views/Location.vue'),
    children: LocationChildren
  },
  {
    path: '/explore',
    meta: { requiresAuth: true },
    component: () => import(/* webpackChunkName: "explore" */ '@/views/Explore.vue'),
    children: ExploreChildren
  },
  {
    path: '/settings',
    meta: { requiresAuth: true },
    component: () => import(/* webpackChunkName: "settings" */ '@/views/Settings.vue'),
    children: SettingsChildren
  },
  {
    path: '/faq',
    name: 'faq',
    meta: {},
    component: () => import(/* webpackChunkName: "faq" */ '@/views/FAQ.vue')
  },
  {
    path: '/about',
    name: 'about',
    meta: {},
    component: () => import(/* webpackChunkName: "about" */ '@/views/About.vue')
  }
]
let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: baseRoute
})
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!getToken()) {
      next({
        path: '/',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.requiresNotAuth)) {
    if (getToken()) {
      next({ name: 'feedActivities' })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
