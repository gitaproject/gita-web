export default {
  friendsNoMore: false,
  friends: [],
  friendsIsLoading: false,
  friendsPage: 0,
  friendsSearch: null,

  isLoading: false,
  friendUsername: null,
  friendRequests: []
}
