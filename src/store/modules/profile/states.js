export default {
  avatarFile: null,
  coverFile: null,

  userId: null,
  profileId: null,
  createdAt: null,
  updatedAt: null,
  birthday: null,
  gender: null,
  about: null,
  avatar: null,
  cover: null,
  lastLogin: null,
  username: null,
  lastName: null,
  email: null,
  isActive: null,
  dateJoined: null,
  phone: null,
  firstName: null,
  phoneVerified: null,
  emailVerified: null
}
