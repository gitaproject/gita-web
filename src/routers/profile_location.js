export default [
  {
    path: '',
    name: 'profileLocationWall',
    component: () => import(/* webpackChunkName: "profileLocation" */ '@/components/profile_location/Wall.vue')
  },
  {
    path: 'about',
    name: 'profileLocationAbout',
    component: () => import(/* webpackChunkName: "profileLocation" */ '@/components/profile_location/About.vue')
  },
  {
    path: 'members',
    name: 'profileLocationMembers',
    component: () => import(/* webpackChunkName: "profileLocation" */ '@/components/profile_location/Members.vue')
  },
  {
    path: 'post',
    name: 'profileLocationPost',
    component: () => import(/* webpackChunkName: "profileLocation" */ '@/views/profile_location/Post.vue'),
    children: [
      {
        path: 'wall',
        name: 'profileLocationPostWall',
        component: () => import(/* webpackChunkName: "profileLocation" */ '@/components/profile_location/post/Wall.vue')
      }
    ]
  },
  {
    path: 'manage',
    name: 'profileLocationManage',
    component: () => import(/* webpackChunkName: "profileLocation" */ '@/views/profile_location/Manage.vue'),
    children: [
      {
        path: 'agendas',
        name: 'profileLocationManageAgenda',
        component: () => import(/* webpackChunkName: "profileLocation" */ '@/components/profile_location/manage/Agenda.vue')
      },
      {
        path: 'members',
        name: 'profileLocationManageMembers',
        component: () => import(/* webpackChunkName: "profileLocation" */ '@/components/profile_location/manage/Member.vue')
      }
    ]
  }
]
