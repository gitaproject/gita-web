import HTTP from '@/utils/apiBase'
import { setToken, removeToken } from '@/utils/apiAuth'
import router from '@/routers'

export default {
  async doLogin ({ commit, state }) {
    let data = {
      'username': state.username,
      'password': state.password
    }
    await HTTP().post(`/auth/login/`, data).then(
      response => {
        setToken(response.data.token)
        commit('setLogged')
        commit('setLayout', 'auth-layout')
        router.push({ name: 'feed' })
      }
    ).catch(
      e => {
        console.log(e.response.data.error[0])
        commit('setLoginMessage', e.response.data.error[0])
        // router.push({ name: 'login' })
      }
    )
  },
  doLogout (context) {
    removeToken()
    context.commit('setLogout')
  }
}
