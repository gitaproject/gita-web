export default [
  {
    path: '',
    name: 'profileGroupWall',
    component: () => import(/* webpackChunkName: "profileGroup" */ '@/components/profile_group/Wall.vue')
  },
  {
    path: 'about',
    name: 'profileGroupAbout',
    component: () => import(/* webpackChunkName: "profileGroup" */ '@/components/profile_group/About.vue')
  },
  {
    path: 'members',
    name: 'profileGroupMembers',
    component: () => import(/* webpackChunkName: "profileGroup" */ '@/components/profile_group/Members.vue')
  },
  {
    path: 'post',
    name: 'profileGroupPost',
    component: () => import(/* webpackChunkName: "profileGroup" */ '@/views/profile_group/Post.vue'),
    children: [
      {
        path: 'wall',
        name: 'profileGroupPostWall',
        component: () => import(/* webpackChunkName: "profileGroup" */ '@/components/profile_group/post/Wall.vue')
      },
      {
        path: 'agenda',
        name: 'profileGroupPostAgenda',
        component: () => import(/* webpackChunkName: "profileGroup" */ '@/components/profile_group/post/Agenda.vue')
      }
    ]
  },
  {
    path: 'manage',
    name: 'profileGroupManage',
    component: () => import(/* webpackChunkName: "profileGroup" */ '@/views/profile_group/Manage.vue'),
    children: [
      {
        path: 'agendas',
        name: 'profileGroupManageAgenda',
        component: () => import(/* webpackChunkName: "profileGroup" */ '@/components/profile_group/manage/Agenda.vue')
      },
      {
        path: 'members',
        name: 'profileGroupManageMembers',
        component: () => import(/* webpackChunkName: "profileGroup" */ '@/components/profile_group/manage/Member.vue')
      }
    ]
  }
]
