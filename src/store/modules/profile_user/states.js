export default {
  otherUser: {
    id: null,
    username: null,
    first_name: null,
    last_name: null,
    full_name: null,
    date_joined: null,
    profile: {
      about: null,
      avatar: null,
      cover: null,
      gender: null,
      birthday: null
    },
    can_request_send: false,
    are_friends: false
  },
  username: ''
}
