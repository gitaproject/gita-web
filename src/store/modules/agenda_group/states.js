export default {
  activityId: 0,
  userActivityIsActivityValid: false,
  activityDuration: '',
  activityTimeStart: '',
  activityActivityName: '',
  activityDescription: '',
  activityGroup: '',
  activityLocation: '',
  activityAgenda: '',
  activityCreatedAt: '',
  activityUpdatedAt: ''
}
