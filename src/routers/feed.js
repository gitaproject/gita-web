export default [
  { path: '', name: 'feed', redirect: 'activities' },
  {
    path: 'activities',
    name: 'feedActivities',
    component: () => import(/* webpackChunkName: "profile" */ '@/components/feed/ActivityList.vue')
  },
  {
    path: 'agendas',
    name: 'feedAgendas',
    component: () => import(/* webpackChunkName: "profile" */ '@/components/feed/AgendaList.vue')
  },
  {
    path: 'posts',
    name: 'feedPosts',
    component: () => import(/* webpackChunkName: "profile" */ '@/components/feed/PostList.vue')
  }
]
