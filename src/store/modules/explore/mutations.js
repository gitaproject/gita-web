export default {
  setSearchKey (state, searchKey) { state.searchKey = searchKey },
  setExplore (state, explore) { state.explore = explore }
}
