import HTTP from '@/utils/apiBase'
import router from '@/routers'

export default {
  async readPersonalActivities ({ commit, state }) {
    commit('Application/startLoading', {}, { root: true })
    if (state.personalActivitiesIsLoading || state.personalActivitiesNoMore) return
    commit('setPersonalActivitiesLoading', true)
    await HTTP().get(`/agenda/activity/?page=${state.personalActivitiesPage + 1}`).then(
      response => {
        commit('addPersonalActivities', response.data)
        state.personalActivitiesNoMore = !response.data['next']
      }
    ).catch(
      e => {
        commit('setPersonalActivitiesLoading', false)
        state.personalActivitiesNoMore = e.response.status === 404
        if (!state.personalActivitiesNoMore) commit('Application/setSnackbarText', 'Fail to retrieve activities.', { root: true })
      }
    ).finally(() => {
      commit('Application/endLoading', {}, { root: true })
    })
  },
  async checkPersonalActivity ({ commit, state }) {
    commit('Application/startLoading', {}, { root: true })
    let data = {
      'activity': {
        'time_start': state.activityTimeStart,
        'duration': state.activityDuration,
        'activity_name': state.activityActivityName,
        'description': state.activityDescription
      }
    }
    if (state.activityExceptId) data['except_id'] = state.activityExceptId
    await HTTP().post(`/agenda/activity/check/`, data).then(
      response => {
        commit('setInvalidPersonalActivities', response.data)
      }
    ).catch(
      e => {
        commit('Application/setSnackbarText', 'Fail to retrieve activity.', { root: true })
      }
    ).finally(() => {
      commit('Application/endLoading', {}, { root: true })
    })
  },
  async createPersonalActivity ({ commit, state, dispatch }) {
    commit('Application/startLoading', {}, { root: true })
    let data = {
      'activity': {
        'time_start': state.activityTimeStart,
        'duration': state.activityDuration,
        'activity_name': state.activityActivityName,
        'description': state.activityDescription
      }
    }
    await HTTP().post(`/agenda/activity/`, data).then(
      response => {
        router.push({ name: 'feedActivities' })
      }
    ).catch(
      e => {
        commit('Application/setSnackbarText', 'Fail to create new activity.', { root: true })
      }
    ).finally(() => {
      commit('Application/endLoading', {}, { root: true })
    })
  },
  async retrievePersonalActivity ({ commit, state }) {
    commit('Application/startLoading', {}, { root: true })
    await HTTP().patch(`/agenda/activity/${state.userActivityId}/`).get(
      response => {
        commit('setPersonalActivity', response.data)
      }
    ).catch(
      e => {
        commit('Application/setSnackbarText', 'Fail to retrieve activity.', { root: true })
      }
    ).finally(() => {
      commit('Application/endLoading', {}, { root: true })
    })
  },
  async updatePersonalActivity ({ commit, state, dispatch }) {
    commit('Application/startLoading', {}, { root: true })
    let data = {
      'activity': {
        'time_start': state.activityTimeStart,
        'duration': state.activityDuration,
        'activity_name': state.activityActivityName,
        'description': state.activityDescription
      }
    }
    await HTTP().patch(`/agenda/activity/${state.userActivityId}/`, data).then(
      response => {
        commit('resetPersonalActivities')
        dispatch('readPersonalActivities')
      }
    ).catch(
      e => {
        commit('setPersonalActivitiesLoading', false)
        commit('Application/setSnackbarText', 'Fail to update activity.', { root: true })
      }
    ).finally(() => {
      commit('Application/endLoading', {}, { root: true })
    })
  },
  async deletePersonalActivity ({ commit, state, dispatch }) {
    commit('Application/startLoading', {}, { root: true })
    await HTTP().delete(`/agenda/activity/${state.userActivityId}/`).then(
      response => {
        commit('resetPersonalActivities')
        dispatch('readPersonalActivities')
      }
    ).catch(
      e => {
        commit('setPersonalActivitiesLoading', false)
        commit('Application/setSnackbarText', 'Fail to delete activity.', { root: true })
      }
    ).finally(() => {
      commit('Application/endLoading', {}, { root: true })
    })
  }
}
