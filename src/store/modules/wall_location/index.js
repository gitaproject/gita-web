import Getters from './getters'
import States from './states'
import Mutations from './mutations'
import Actions from './actions'

export default {
  namespaced: true,
  state: States,
  getters: Getters,
  actions: Actions,
  mutations: Mutations
}
