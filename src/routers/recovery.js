export default [
  {
    path: '',
    name: 'recoveryUser',
    component: () => import(/* webpackChunkName: "recovery" */ '@/components/recovery/RecoveryUser.vue')
  },
  {
    path: 'validate/:uuid/:token',
    name: 'validateRecovery',
    component: () => import(/* webpackChunkName: "recovery" */ '@/components/recovery/ValidateRecovery.vue')
  },
  {
    path: 'reset',
    name: 'resetPassword',
    component: () => import(/* webpackChunkName: "recovery" */ '@/components/recovery/ResetPassword.vue')
  }
]
