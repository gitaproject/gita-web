export default [
  {
    path: '',
    name: 'exploreMain',
    component: () => import(/* webpackChunkName: "explore" */ '@/components/explore/Main.vue')
  },
  {
    path: 'user',
    name: 'exploreUser',
    component: () => import(/* webpackChunkName: "explore" */ '@/components/explore/User.vue')
  },
  {
    path: 'group',
    name: 'exploreGroup',
    component: () => import(/* webpackChunkName: "explore" */ '@/components/explore/Group.vue')
  },
  {
    path: 'location',
    name: 'exploreLocation',
    component: () => import(/* webpackChunkName: "explore" */ '@/components/explore/Location.vue')
  }
]
