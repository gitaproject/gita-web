export default {
  setLocationsLoading (state, value) {
    state.locationsIsLoading = value
  },
  setLocationsSearch (state, value) {
    state.locationsSearch = value
  },
  addLocations (state, locations) {
    state.locations = state.locations.concat(locations.results)
    state.locationsPage += 1
    state.locationsIsLoading = false
  },
  resetLocations (state) {
    state.locations = []
    state.locationsPage = 0
    state.locationsIsLoading = false
    state.locationsNoMore = false
  }
}
