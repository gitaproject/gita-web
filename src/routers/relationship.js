export default [
  {
    path: 'requests',
    name: 'relationshipFriendRequests',
    component: () => import(/* webpackChunkName: "relationship" */ '@/components/relationship/FriendRequests.vue')
  },
  {
    path: 'friends',
    name: 'relationshipFriends',
    component: () => import(/* webpackChunkName: "relationship" */ '@/components/relationship/FriendList.vue')
  }
]
