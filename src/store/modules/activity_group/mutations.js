export default {
  setGroupActivitiesLoading (state, value) {
    state.groupActivitiesIsLoading = value
  },
  addGroupActivities (state, groupActivities) {
    state.groupActivities = state.groupActivities.concat(groupActivities.results)
    state.groupActivitiesPage += 1
    state.groupActivitiesIsLoading = false
  },
  resetGroupActivities (state) {
    state.groupActivities = []
    state.groupActivitiesPage = 1
    state.groupActivitiesIsLoading = false
  },
  setGroupActivity (state, activity) {
    state.userActivityIsActivityValid = activity['is_activity_valid']
    state.activityId = activity['activity']['id']
    state.activityDuration = activity['activity']['duration']
    state.activityTimeStart = activity['activity']['time_start']
    state.activityActivityName = activity['activity']['activity_name']
    state.activityDescription = activity['activity']['description']
    state.activityGroup = activity['activity']['group']
    state.activityLocation = activity['activity']['location']
    state.activityAgenda = activity['activity']['agenda']
    state.activityCreatedAt = activity['activity']['created_at']
    state.activityUpdatedAt = activity['activity']['updated_at']
  },
  setUserActivityId (state, userActivityId) { state.userActivityId = userActivityId },
  setUserActivityIsActivityValid (state, userActivityIsActivityValid) { state.userActivityIsActivityValid = userActivityIsActivityValid },
  setActivityId (state, activityId) { state.activityId = activityId },
  setActivityDuration (state, activityDuration) { state.activityDuration = activityDuration },
  setActivityTimeStart (state, activityTimeStart) { state.activityTimeStart = activityTimeStart },
  setActivityActivityName (state, activityActivityName) { state.activityActivityName = activityActivityName },
  setActivityDescription (state, activityDescription) { state.activityDescription = activityDescription },
  setActivityGroup (state, activityGroup) { state.activityGroup = activityGroup },
  setActivityLocation (state, activityLocation) { state.activityLocation = activityLocation },
  setActivityAgenda (state, activityAgenda) { state.activityAgenda = activityAgenda },
  setActivityCreatedAt (state, activityCreatedAt) { state.activityCreatedAt = activityCreatedAt },
  setActivityUpdatedAt (state, activityUpdatedAt) { state.activityUpdatedAt = activityUpdatedAt }
}
