export default {
  userActivityIsActivityValid: state => state.userActivityIsActivityValid,
  activityDuration: state => state.activityDuration,
  activityTimeStart: state => state.activityTimeStart,
  activityActivityName: state => state.activityActivityName,
  activityDescription: state => state.activityDescription,
  activityGroup: state => state.activityGroup,
  activityLocation: state => state.activityLocation,
  activityAgenda: state => state.activityAgenda,
  activityCreatedAt: state => state.activityCreatedAt,
  activityUpdatedAt: state => state.activityUpdatedAt
}
