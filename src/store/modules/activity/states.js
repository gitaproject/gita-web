export default {
  personalActivitiesNoMore: false,
  personalActivities: [],
  personalActivitiesIsLoading: false,
  personalActivitiesPage: 0,

  invalidPersonalActivities: [],
  invalidPersonalActivitiesCount: 0,

  activityExceptId: 0,

  userActivityId: null,
  userActivityIsActivityValid: false,
  activityId: null,
  activityDuration: 0,
  activityTimeStart: new Date().toISOString(),
  activityActivityName: '',
  activityDescription: '',
  activityGroup: '',
  activityLocation: '',
  activityAgenda: '',
  activityCreatedAt: '',
  activityUpdatedAt: ''
}
