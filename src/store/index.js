import Vue from 'vue'
import Vuex from 'vuex'

import AccountRecovery from './modules/account_recovery'
import Agenda from './modules/agenda'
import Activity from './modules/activity'
import Application from './modules/application'
import Authentication from './modules/authentication'
import Explore from './modules/explore'
import Feed from './modules/feed'
import Group from './modules/group'
import Location from './modules/location'
import Notification from './modules/notification'
import Profile from './modules/profile'
import ProfileUser from './modules/profile_user'
import Registration from './modules/registration'
import Relationship from './modules/relationship'
import Wall from './modules/wall'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    AccountRecovery,
    Agenda,
    Activity,
    Application,
    Authentication,
    Explore,
    Feed,
    Group,
    Location,
    Notification,
    Profile,
    ProfileUser,
    Registration,
    Relationship,
    Wall
  }
})
