export default {
  personalActivitiesNoMore: state => state.personalActivitiesNoMore,
  personalActivities: state => state.personalActivities,
  personalActivitiesIsLoading: state => state.personalActivitiesIsLoading,
  personalActivitiesPage: state => state.personalActivitiesPage,

  invalidPersonalActivities: state => state.invalidPersonalActivities,
  invalidPersonalActivitiesCount: state => state.invalidPersonalActivitiesCount,

  activityExceptId: state => state.activityExceptId,

  userActivityId: state => state.userActivityId,
  userActivityIsActivityValid: state => state.userActivityIsActivityValid,
  activityId: state => state.activityId,
  activityDuration: state => state.activityDuration,
  activityTimeStart: state => state.activityTimeStart,
  activityActivityName: state => state.activityActivityName,
  activityDescription: state => state.activityDescription,
  activityGroup: state => state.activityGroup,
  activityLocation: state => state.activityLocation,
  activityAgenda: state => state.activityAgenda,
  activityCreatedAt: state => state.activityCreatedAt,
  activityUpdatedAt: state => state.activityUpdatedAt
}
