import HTTP from '@/utils/apiBase'

export default {
  photoSelected (evt) {
    let input = evt.target
    if (input.files && input.files[0]) {
      this.pictureFile = input.files[0]
      // create a new FileReader to read this image and convert to base64 format
      let reader = new FileReader()
      // Define a callback function to run, when FileReader finishes its job
      reader.onload = (e) => {
        // Note: arrow function used here, so that "this.imageData" refers to the imageData of Vue component
        // Read image as base64 and set to imageData
        this.pictureFileURL = e.target.result
      }
      // Start the reader job - read file as a data url (base64 format)
      reader.readAsDataURL(input.files[0])
    }
  },
  async readUserProfile ({ commit, dispatch }) {
    await HTTP().get(`/social/profile/`).then(
      response => {
        commit('setUserProfile', response.data)
      }
    ).catch(
      e => {
        dispatch('Authentication/doLogout', null, { root: true })
      }
    )
  },
  async updateProfilePicture ({ state, commit, dispatch }) {
    let formData = new FormData()
    formData.append('avatar', state.avatarFile)
    let HttpHeader = {
      dataType: 'json',
      contentType: 'multipart/form-data'
    }
    await HTTP().put(`/social/profile/avatar/`, formData, HttpHeader).then(
      response => {
        commit('setAvatar', response.data['avatar'])
      }
    ).catch(
      e => {
        dispatch('Authentication/doLogout', null, { root: true })
      }
    )
  },
  async updateCoverPicture ({ state, commit, dispatch }) {
    let formData = new FormData()
    formData.append('cover', state.coverFile)
    let HttpHeader = {
      dataType: 'json',
      contentType: 'multipart/form-data'
    }
    await HTTP().put(`/social/profile/cover/`, formData, HttpHeader).then(
      response => {
        commit('setCover', response.data['cover'])
      }
    ).catch(
      e => {
        dispatch('Authentication/doLogout', null, { root: true })
      }
    )
  }
}
