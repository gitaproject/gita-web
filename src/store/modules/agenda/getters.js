export default {
  personalAgendasNoMore: state => state.personalAgendasNoMore,
  invalidPersonalAgendas: state => state.invalidPersonalAgendas,
  invalidPersonalAgendasCount: state => state.invalidPersonalAgendasCount,
  personalAgendas: state => state.personalAgendas,
  personalAgendasIsLoading: state => state.personalAgendasIsLoading,
  personalAgendasPage: state => state.personalAgendasPage,

  agendaId: state => state.agendaId,
  agendaName: state => state.agendaName,
  description: state => state.description,
  loop: state => state.loop,
  rangeTimeStart: state => state.rangeTimeStart,
  rangeTimeUntil: state => state.rangeTimeUntil,
  loopInterlude: state => state.loopInterlude,
  locations: state => state.locations,

  activities: state => state.activities,
  activityName: (state) => (index) => { return state.activities[index].activity_name },
  activityDescription: (state) => (index) => { return state.activities[index].description },
  activityDuration: (state) => (index) => { return state.activities[index].duration },
  activityHosts: (state) => (index) => { return state.activities[index].hosts },
  activityMembers: (state) => (index) => { return state.activities[index].members },
  activityLocation: (state) => (index) => { return state.activities[index].location }
}
