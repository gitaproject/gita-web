const authTokenKey = 'gita_auth_token'

export function getToken () {
  return localStorage.getItem(authTokenKey)
}

export function setToken (value) {
  return localStorage.setItem(authTokenKey, value)
}

export function removeToken () {
  return localStorage.removeItem(authTokenKey)
}
