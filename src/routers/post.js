export default [
  { path: '', name: 'post', redirect: 'activity' },
  {
    path: 'wall',
    name: 'postPersonalWall',
    component: () => import(/* webpackChunkName: "post" */ '@/components/post/forms/PersonalWall.vue')
  },
  {
    path: 'activity',
    name: 'postPersonalActivity',
    component: () => import(/* webpackChunkName: "post" */ '@/components/post/forms/PersonalActivity.vue')
  },
  {
    path: 'agenda',
    name: 'postPersonalAgenda',
    component: () => import(/* webpackChunkName: "post" */ '@/components/post/forms/PersonalAgenda.vue')
  }
]
