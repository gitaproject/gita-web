import { getToken } from '@/utils/apiAuth'

export default {
  registerInfo: '',
  resendTokenInfo: '',
  isLogged: !!getToken(),
  isValid: false,
  username: '',
  password: '',
  token: '',
  loginMessage: ''
}
