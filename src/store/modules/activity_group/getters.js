export default {
  groupActivities: state => state.groupActivities,
  groupActivitiesIsLoading: state => state.groupActivitiesIsLoading,
  groupActivitiesPage: state => state.groupActivitiesPage,

  invalidGroupActivities: state => state.invalidGroupActivities,
  invalidGroupActivitiesCount: state => state.invalidGroupActivitiesCount,

  activityExceptId: state => state.activityExceptId,

  userActivityId: state => state.userActivityId,
  userActivityIsActivityValid: state => state.userActivityIsActivityValid,
  activityId: state => state.activityId,
  activityDuration: state => state.activityDuration,
  activityTimeStart: state => state.activityTimeStart,
  activityActivityName: state => state.activityActivityName,
  activityDescription: state => state.activityDescription,
  activityGroup: state => state.activityGroup,
  activityLocation: state => state.activityLocation,
  activityAgenda: state => state.activityAgenda,
  activityCreatedAt: state => state.activityCreatedAt,
  activityUpdatedAt: state => state.activityUpdatedAt
}
