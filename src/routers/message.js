export default [
  {
    path: '',
    name: 'messageList',
    component: () => import(/* webpackChunkName: "message" */ '@/components/message/List.vue')
  },
  {
    path: ':username',
    name: 'messageFromUser',
    component: () => import(/* webpackChunkName: "message" */ '@/components/message/FromUser.vue')
  }
]
