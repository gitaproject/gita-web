export default {
  isLogged: state => state.isLogged,
  loginMessage: state => state.loginMessage
}
