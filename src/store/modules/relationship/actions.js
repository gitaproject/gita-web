import HTTP from '@/utils/apiBase'
import router from '@/routers'

export default {
  async getFriends ({ state, commit }) {
    if (state.friendsIsLoading || state.friendsNoMore) return
    commit('Application/startLoading', {}, { root: true })
    let params = {
      page: state.friendsPage + 1,
      search: state.friendsSearch
    }
    state.friendsIsLoading = true
    await HTTP().get(`/relationship/friendship/`, { params: params }).then(
      response => {
        commit('addFriends', response.data)
        state.friendsNoMore = !response.data.next
      }
    ).catch(
      e => {
        if (!state.locationsNoMore) {
          commit('Application/setSnackbarText', 'Fail to load friends list.', { root: true })
          router.push({ name: 'feed' })
        }
      }
    ).finally(() => {
      state.friendsIsLoading = false
      commit('Application/endLoading', {}, { root: true })
    })
  },
  async getOtherFriends ({ state, commit, rootGetters }) {
    commit('Application/startLoading', {}, { root: true })
    let otherUsername = rootGetters['ProfileUser/username']
    await HTTP().get(`/relationship/friendship/other/${otherUsername}/`).then(
      response => {
        state.friends = response.data.results
      }
    ).catch(
      e => {
        commit('Application/setSnackbarText', 'Fail to load friends list.', { root: true })
        router.push({ name: 'feed' })
      }
    ).finally(() => {
      commit('Application/endLoading', {}, { root: true })
    })
  },
  async getFriendRequests ({ state, commit }) {
    commit('Application/startLoading', {}, { root: true })
    await HTTP().get(`/relationship/friendship/friend-requests/`).then(
      response => {
        state.friendRequests = response.data.results
      }
    ).catch(
      e => {
        commit('Application/setSnackbarText', 'Fail to load friend requests list.', { root: true })
        router.push({ name: 'feed' })
      }
    ).finally(() => {
      commit('Application/endLoading', {}, { root: true })
    })
  },
  async postAddFriend ({ state, commit }) {
    commit('Application/startLoading', {}, { root: true })
    let data = {
      username: state.friendUsername
    }
    await HTTP().post(`/relationship/friendship/add/`, data).then(
      response => {
        state.friendUsername = null
        commit('Application/setSnackbarText', response.data.messages, { root: true })
      }
    ).catch(
      e => {
        commit('Application/setSnackbarText', e.response.data.messages, { root: true })
      }
    ).finally(() => {
      commit('Application/endLoading', {}, { root: true })
    })
  },
  async deleteFriendRequest ({ state, commit }) {
    commit('Application/startLoading', {}, { root: true })
    let data = {
      username: state.friendUsername
    }
    await HTTP().delete(`/relationship/friendship/cancel-request/`, data).then(
      response => {
        state.friendUsername = null
      }
    ).catch(
      e => {
        commit('Application/setSnackbarText', 'Fail to cancel friend request.', { root: true })
      }
    ).finally(() => {
      commit('Application/endLoading', {}, { root: true })
    })
  },
  async doSomething ({ state, rootState, commit, dispatch, getters, rootGetters }) {
    let data = {
      'key': 'value'
    }
    await HTTP().post(`/api/url/`, data).then(
      response => {
        // get state
        let samplePayload = state.sampleState
        let sampleOtherPayload = rootState['ModuleNamespace/somethingElse']
        // retrieve state using getters
        samplePayload = getters.sampleState
        sampleOtherPayload = rootGetters['ModuleNamespace/somethingElse']
        // call mutation
        commit('ModuleNamespace/setSomethingElse', samplePayload, { root: true })
        // call action
        dispatch('ModuleNamespace/doSomethingElse', sampleOtherPayload, { root: true })
      }
    ).catch(
      e => {
        router.push({ name: 'feed' })
      }
    )
  }
}
